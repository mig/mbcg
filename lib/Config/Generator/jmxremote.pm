#+##############################################################################
#                                                                              #
# File: Config/Generator/jmxremote.pm                                          #
#                                                                              #
# Description: JMX remote abstraction                                          #
#                                                                              #
#-##############################################################################

# $Id: jmxremote.pm 2077 2014-05-16 06:03:20Z c0ns $

#
# module definition
#

package Config::Generator::jmxremote;
use strict;
use warnings;

#
# used modules
#

use No::Worries::Die qw(dief);
use No::Worries::Export qw(export_control);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);

#
# constants
#

use constant DEF_PORT => {
    type => "integer",
    min => 1024,
    max => 65535,
    optional => "incfg",
};

use constant REQ_ROLE => {
    type => "string",
    match => qr/^(readonly|readwrite)$/,
};

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/jmxremote/user" schema
    register_schema("/jmxremote/user", {
        type => "struct",
        fields => {
            password => REQ_STRING,
            role => REQ_ROLE,
        },
    });
    # define the "/jmxremote" schema
    register_schema("/jmxremote", {
        type => "struct",
        fields => {
            access => DEF_NAME,
            password => DEF_NAME,
            rmiregistry => DEF_PORT,
            rmiserver => DEF_PORT,
            user => {
                type => "table(valid(/jmxremote/user))",
                min => 1,
                match => qr/^\w+$/,
            },
        },
    });
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $cfg = $Config{jmxremote};
    return unless $cfg;
    $cfg->{access} ||= "jmx.access";
    $cfg->{password} ||= "jmx.password";
    $cfg->{rmiregistry} ||= 1099;
    $cfg->{rmiserver} ||= $cfg->{rmiregistry} + 1;
}

#
# generate the access file contents
#

sub generate_access () {
    my($cfg, $contents);

    $cfg = $Config{jmxremote};
    dief("jmxremote not configured!") unless $cfg;
    $contents = "#\n# jmxremote access file\n#\n";
    foreach my $name (sort(keys(%{ $cfg->{user} }))) {
        $contents .= sprintf("%s %s\n", $name, $cfg->{user}{$name}{role});
    }
    return($contents);
}

#
# generate the password file contents
#

sub generate_password () {
    my($cfg, $contents);

    $cfg = $Config{jmxremote};
    dief("jmxremote not configured!") unless $cfg;
    $contents = "#\n# jmxremote password file\n#\n";
    foreach my $name (sort(keys(%{ $cfg->{user} }))) {
        $contents .= sprintf("%s %s\n", $name, $cfg->{user}{$name}{password});
    }
    return($contents);
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{"generate_$_"}++, qw(access password));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

#
# registration
#

init();
register_hook("check", \&check);

1;
