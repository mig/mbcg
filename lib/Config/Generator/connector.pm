#+##############################################################################
#                                                                              #
# File: Config/Generator/connector.pm                                          #
#                                                                              #
# Description: service connector abstraction                                   #
#                                                                              #
#-##############################################################################

# $Id: connector.pm 2077 2014-05-16 06:03:20Z c0ns $

#
# module definition
#

package Config::Generator::connector;
use strict;
use warnings;

#
# used modules
#

use Config::Validator qw(is_true);
use No::Worries::Die qw(dief);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);

#
# constants
#

use constant DEF_ADDR => {
    type => [ "ipv4", "ipv6" ],
    optional => "incfg",
};

use constant REQ_PORT => {
    type => "integer",
    min => 1,
    max => 65535,
};

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/connector/entry" schema
    register_schema("/connector/entry", {
        type => "struct",
        fields => {
            protocol => REQ_NAME,
            port => REQ_PORT,
            bind => DEF_ADDR,
            ssl => DEF_BOOLEAN,
            udp => DEF_BOOLEAN,
        },
    });
    # define the "/connector" schema
    register_schema("/connector", {
        type => "table(valid(/connector/entry))",
        min => 1,
        match => qr/^[\w\-\+]+$/,
    });
    # declare the "/connector" subtree as mandatory
    mandatory_subtree("/connector");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my(%seen, $unique);

    foreach my $connector (values(%{ $Config{connector} })) {
        $connector->{bind} ||= "0.0.0.0";
        $connector->{ssl} ||= "false";
        $connector->{udp} ||= "false";
        $unique = $connector->{port} . "/" .
            (is_true($connector->{udp}) ? "udp" : "tcp");
        dief("duplicate connector: %s", $unique) if $seen{$unique}++;
    }
}

#
# registration
#

init();
register_hook("check", \&check);

1;
