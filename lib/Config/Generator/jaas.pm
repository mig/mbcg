#+##############################################################################
#                                                                              #
# File: Config/Generator/jaas.pm                                               #
#                                                                              #
# Description: JAAS abstraction                                                #
#                                                                              #
#-##############################################################################

# $Id: jaas.pm 2685 2016-06-27 07:10:35Z c0ns $

#
# module definition
#

package Config::Generator::jaas;
use strict;
use warnings;

#
# used modules
#

use No::Worries::Die qw(dief);
use No::Worries::DN qw(dn_parse dn_string);
use No::Worries::Export qw(export_control);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
# dependencies
use Config::Generator::user qw();

#
# constants
#

use constant DEF_DN_FORMAT => {
    type => "string",
    match => qr/^(java|rfc2253)$/,
    optional => "incfg",
};

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/jaas" schema
    register_schema("/jaas", {
        type => "struct",
        fields => {
            debug     => DEF_BOOLEAN,
            reload    => DEF_BOOLEAN,
            config    => DEF_NAME,
            users     => DEF_NAME,
            groups    => DEF_NAME,
            dns       => DEF_NAME,
            dn_format => DEF_DN_FORMAT,
        },
    });
    # declare the "/jaas" subtree as mandatory
    mandatory_subtree("/jaas");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $Config{jaas} ||= {};
    $cfg = $Config{jaas};
    $cfg->{debug} ||= "false";
    $cfg->{reload} ||= "false";
    $cfg->{config} ||= "login.config";
    $cfg->{users} ||= "users.properties";
    $cfg->{groups} ||= "groups.properties";
    $cfg->{dns} ||= "dns.properties";
    $cfg->{dn_format} ||= "java";
}

#
# generate the users file contents
#

sub generate_users (;$) {
    my($ignore) = @_;
    my($contents, $entry);

    $contents = "#\n# JAAS users\n#\n";
    foreach my $name (sort(keys(%{ $Config{user} }))) {
        next if $ignore and $name =~ $ignore;
        $entry = $Config{user}{$name};
        next unless defined($entry->{password});
        $contents .= sprintf("%s=%s\n", $name, $entry->{password});
    }
    return($contents);
}

#
# generate the groups file contents
#

sub generate_groups (;$) {
    my($ignore) = @_;
    my($contents, $entry, %seen);

    $contents = "#\n# JAAS groups\n#\n";
    foreach my $name (keys(%{ $Config{user} })) {
        next if $ignore and $name =~ $ignore;
        $entry = $Config{user}{$name};
        next unless $entry->{group};
        foreach my $tmp (@{ $entry->{group} }) {
            $seen{$tmp}{$name}++;
        }
    }
    foreach my $name (sort(keys(%seen))) {
        next if $ignore and $name =~ $ignore;
        $entry = join(",", sort(keys(%{ $seen{$name} })));
        $contents .= sprintf("%s=%s\n", $name, $entry);
    }
    return($contents);
}

#
# generate the dns file contents
#

sub generate_dns (;$) {
    my($ignore) = @_;
    my($contents, $entry, $dn);

    $contents = "#\n# JAAS DNs\n#\n";
    foreach my $name (sort(keys(%{ $Config{user} }))) {
        next if $ignore and $name =~ $ignore;
        $entry = $Config{user}{$name};
        next unless defined($entry->{dn});
        $dn = dn_parse($entry->{dn});
        if ($Config{jaas}{dn_format} eq "java") {
            $dn = dn_string($dn, No::Worries::DN::FORMAT_JAVA);
        } elsif ($Config{jaas}{dn_format} eq "rfc2253") {
            $dn = dn_string($dn, No::Worries::DN::FORMAT_RFC2253);
        } else {
            dief("unexpected JAAS DN format: %s", $Config{jaas}{dn_format});
        }
        $contents .= sprintf("%s=%s\n", $name, $dn);
    }
    return($contents);
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{"generate_$_"}++, qw(users groups dns));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

#
# registration
#

init();
register_hook("check", \&check);

1;
