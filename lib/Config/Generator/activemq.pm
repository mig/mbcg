#+##############################################################################
#                                                                              #
# File: Config/Generator/activemq.pm                                           #
#                                                                              #
# Description: ActiveMQ messaging broker abstraction                           #
#                                                                              #
#-##############################################################################

# $Id: activemq.pm 2705 2016-07-14 13:17:53Z c0ns $

#
# module definition
#

package Config::Generator::activemq;
use strict;
use warnings;

#
# used modules
#

use Config::Validator qw(is_false is_true);
use No::Worries::Die qw(dief);
use No::Worries::DN qw(dn_parse dn_string);
use No::Worries::Log qw(log_debug);
use Config::Generator qw(%Config);
use Config::Generator::Crontab qw(format_crontab);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Random qw(random_seed);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use Config::Generator::Util qw(MB GB format_profile list_of);
use Config::Generator::XML qw(*);
# dependencies
use Config::Generator::activemq::connector qw(*);
use Config::Generator::activemq::ssl qw(*);
use Config::Generator::activemq::storage qw(*);
use Config::Generator::broker qw();
use Config::Generator::host qw();
use Config::Generator::jaas qw();
use Config::Generator::java qw();
use Config::Generator::jmxremote qw();
use Config::Generator::quota qw();
use Config::Generator::user qw();

#
# constants
#

use constant BEANS_XMLNS =>
    "http://www.springframework.org/schema/beans";

use constant BROKER_XMLNS =>
    "http://activemq.apache.org/schema/core";

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/activemq" schema
    register_schema("/activemq", {
        type => "struct",
        fields => {
            # simplified <authorizationPlugin> element
            authorizationEntries => OPT_STRING,
            # <broker> attributes
            advisorySupport => OPT_BOOLEAN,
            mbeanInvocationTimeout => OPT_INTEGER,
            networkConnectorStartAsync => OPT_BOOLEAN,
            offlineDurableSubscriberTimeout => OPT_INTEGER,
            offlineDurableSubscriberTaskSchedule => OPT_INTEGER,
            persistent => OPT_BOOLEAN,
            rejectDurableConsumers => OPT_BOOLEAN,
            schedulePeriodForDestinationPurge => OPT_INTEGER,
            tmpDataDirectory => OPT_PATH,
            # <destinationInterceptors> element
            destinationInterceptors => OPT_STRING,
            # simplified <destinationPolicy> element
            policyEntries => OPT_STRING,
            # <managementContext> attributes
            allowRemoteAddressInMBeanNames => OPT_BOOLEAN,
            # <networkConnectors> element
            networkConnectors => OPT_STRING,
            # <systemUsage> attributes and sub-elements
            sendFailIfNoSpace => OPT_INTEGER, # with special treatment!
            memoryUsage => DEF_STRING,
            storeUsage => DEF_STRING,
            tempUsage => DEF_STRING,
            # Java Service Wrapper attributes
            UseDedicatedTaskRunner => DEF_BOOLEAN,
            # other parameters (not directly matching attributes or elements)
            configurationCheckPeriod => OPT_INTEGER,
            declaredQueue => OPT_STRING_LIST,
            declaredTopic => OPT_STRING_LIST,
            jar => DEF_STRING,
            maximumTtl => OPT_INTEGER,
            splitDataDirectory => OPT_PATH,
        },
    });
    # declare the "/activemq" subtree as mandatory
    mandatory_subtree("/activemq");
    # declare the templates we use
    declare_template(map("activemq/$_", qw(activemq brkpurge jetty
        jolokia-access log4j login service update-keystore wrapper)));
}

#
# configuration checking (including setting the defaults)
#

sub check_usage () {
    my($cfg, $size);

    $cfg = $Config{activemq};
    unless ($cfg->{memoryUsage}) {
        # default memoryUsage is 2/3 of maximum heap
        $size = $Config{quota}{memory} * $Config{quota}{memory_heap} * 2 / 3;
        $cfg->{memoryUsage} = int($size / MB) . "mb";
    }
    unless ($cfg->{storeUsage}) {
        # default storeUsage is 2/3 of disk quota
        $size = $Config{quota}{disk} * 2 / 3;
        $cfg->{storeUsage} = int($size / MB) . "mb";
    }
    unless ($cfg->{tempUsage}) {
        # default tempUsage is 1/3 of disk quota
        $size = $Config{quota}{disk} / 3;
        $cfg->{tempUsage} = int($size / MB) . "mb";
    }
}

sub check () {
    my($cfg);

    # type/version checks
    dief("unsupported broker version: %s %s",
         $Config{broker}{type}, $Config{broker}{version})
        unless $Config{broker}{type} eq "ActiveMQ"
           and $Config{broker}{version} =~ /^5\.\d+$/;
    # defaults
    $Config{activemq} ||= {};
    $cfg = $Config{activemq};
    $cfg->{UseDedicatedTaskRunner} ||= "false";
    $cfg->{jar} ||= "activemq.jar";
    $cfg->{tmpDataDirectory} ||= "$cfg->{splitDataDirectory}/tempdb"
        if $cfg->{splitDataDirectory};
    # other checks/defaults
    check_usage();
    # seed the pseudo-random generator
    random_seed("activemq-$Config{broker}{name}-$Config{host}{fqdn}");
    # the jetty connector requires a jetty user
    dief("missing jetty user: required by the jetty connector")
        if $Config{connector}{jetty} and not $Config{user}{jetty};
}

#
# helpers
#

sub generate_beans_entry ($$) {
    my($key, $value) = @_;

    return(xml_element("entry",
        { xmlns => BEANS_XMLNS, key => $key, value => $value }
    ));
}

sub parse_given_xml ($) {
    my($name) = @_;

    return(xml_parse("<$name>$Config{activemq}{$name}</$name>"));
}

#
# confdir/activemq.xml generation
#

sub generate_destinationInterceptors () {
    return(
        xml_comment("destination configuration"),
        parse_given_xml("destinationInterceptors"),
    );
}

sub generate_destinationPolicy () {
    return(
        xml_comment("destination policies"),
        xml_wrap(qw(destinationPolicy policyMap),
            parse_given_xml("policyEntries"),
        ),
    );
}

sub generate_destinations () {
    my(@body);

    foreach my $name (sort(list_of($Config{activemq}{declaredQueue}))) {
        push(@body, xml_element("queue", { physicalName => $name }));
    }
    foreach my $name (sort(list_of($Config{activemq}{declaredTopic}))) {
        push(@body, xml_element("topic", { physicalName => $name }));
    }
    return(
        xml_comment("destinations to be created during startup"),
        xml_wrap(qw(destinations), @body),
    );
}

sub generate_managementContext () {
    my($cfg, %attr, @body);

    $cfg = $Config{jmxremote};
    if ($cfg) {
        $attr{createConnector} = "true";
        $attr{connectorHost} = "0.0.0.0";
        $attr{connectorPort} = $cfg->{rmiregistry};
        $attr{rmiServerPort} = $cfg->{rmiserver};
        push(@body, xml_element(
            "property", { xmlns => BEANS_XMLNS, name => "environment" },
                 xml_element("map", { xmlns => BEANS_XMLNS },
                     generate_beans_entry(
                         "jmx.remote.x.password.file",
                         "\${activemq.conf}/$cfg->{password}",
                     ),
                     generate_beans_entry(
                         "jmx.remote.x.access.file",
                         "\${activemq.conf}/$cfg->{access}",
                     ),
                 ),
        ));
    } else {
        $attr{createConnector} = "false";
    }
    foreach my $name (qw(allowRemoteAddressInMBeanNames)) {
        $attr{$name} = $Config{activemq}{$name} if $Config{activemq}{$name};
    }
    return(xml_wrap(qw(managementContext),
        xml_element("managementContext", \%attr, @body),
    ));
}

sub generate_networkConnectors () {
    return(
        xml_comment("network of brokers configuration"),
        parse_given_xml("networkConnectors"),
    );
}

sub generate_plugins () {
    my(%plugin, @body, $value);

    # authorizationPlugin is used for access control
    if ($Config{activemq}{authorizationEntries}) {
        $value = xml_wrap(qw(map authorizationMap),
            parse_given_xml("authorizationEntries"),
        );
        $plugin{authorizationPlugin} = [
            "access control", undef, [ $value ],
        ];
    }
    # jaasDualAuthenticationPlugin is (always) used in login.config
    $plugin{jaasDualAuthenticationPlugin} = [
        "enable dual authentication via the corresponding plugin",
        {
            configuration => "activemq-domain",
            sslConfiguration => "activemq-ssl-domain",
        },
    ];
    # runtimeConfigurationPlugin is optionally used
    $value = $Config{activemq}{configurationCheckPeriod};
    if (defined($value)) {
        $plugin{runtimeConfigurationPlugin} = [
            "enable configuration reloading",
            { checkPeriod => $value },
        ];
    }
    # timeStampingBrokerPlugin is used when enforcing a maximum TTL
    $value = $Config{activemq}{maximumTtl};
    if ($value) {
        $plugin{timeStampingBrokerPlugin} = [
            "enforce a maximum time-to-live",
            {
                ttlCeiling => $value,
                zeroExpirationOverride => $value,
            },
        ];
    }
    # massage all that...
    foreach my $name (sort(keys(%plugin))) {
        push(@body, xml_comment($plugin{$name}[0]))
            if $plugin{$name}[0];
        push(@body, xml_element($name, $plugin{$name}[1],
            $plugin{$name}[2] ? @{ $plugin{$name}[2] } : ()));
    }
    return(xml_wrap(qw(plugins), @body));
}

sub generate_systemUsage () {
    my($value, %attr, @body);

    $value = $Config{activemq}{sendFailIfNoSpace};
    if (not $value) {
        # use defaults
    } elsif ($value < 0) {
        $attr{sendFailIfNoSpace} = "true";
    } else {
        $attr{sendFailIfNoSpaceAfterTimeout} = $value;
    }
    foreach my $name (qw(memoryUsage storeUsage tempUsage)) {
        push(@body, xml_wrap($name,
            xml_element($name, { limit => $Config{activemq}{$name} }),
        ));
    }
    return(xml_wrap("systemUsage", xml_element("systemUsage", \%attr, @body)));
}

sub generate_activemq () {
    my(%xmlopt, %map, %attr, @body, $path);

    %xmlopt = (indent => 1);
    foreach my $name (qw(broker kahaDB networkConnector sslContext)) {
        $xmlopt{split}{$name}++;
    }
    # generate the configuration file
    $attr{useJmx} = "true";
    $attr{populateJMSXUserID} = "true";
    $attr{xmlns} = BROKER_XMLNS;
    $attr{brokerName} = "\${activemq.brokername}";
    $attr{brokerId} = $attr{brokerName};
    $attr{dataDirectory} = $Config{broker}{datadir};
    $attr{start} = "false"
        if defined($Config{activemq}{configurationCheckPeriod});
    foreach my $name (qw(
        advisorySupport
        mbeanInvocationTimeout
        networkConnectorStartAsync
        offlineDurableSubscriberTimeout
        offlineDurableSubscriberTaskSchedule
        persistent
        rejectDurableConsumers
        schedulePeriodForDestinationPurge
        tmpDataDirectory
    )) {
        next unless defined($Config{activemq}{$name});
        $attr{$name} = $Config{activemq}{$name};
    }
    push(@body, xml_blank());
    push(@body, generate_destinationInterceptors(), xml_blank())
        if $Config{activemq}{destinationInterceptors};
    push(@body, generate_destinationPolicy(), xml_blank())
        if $Config{activemq}{policyEntries};
    push(@body, generate_destinations(), xml_blank())
        if $Config{activemq}{declaredQueue} or $Config{activemq}{declaredTopic};
    push(@body, generate_managementContext(), xml_blank());
    push(@body, generate_networkConnectors(), xml_blank())
        if $Config{activemq}{networkConnectors};
    push(@body, generate_persistenceAdapter(), xml_blank())
        unless is_false($Config{activemq}{persistent});
    push(@body, generate_plugins(), xml_blank());
    push(@body, generate_sslContext(), xml_blank())
        if needs_ssl();
    push(@body, generate_systemUsage(), xml_blank());
    push(@body, generate_tempDataStore(), xml_blank())
        if $Config{activemq}{splitDataDirectory}
           and not is_false($Config{activemq}{persistent});
    push(@body, generate_transportConnectors(), xml_blank());
    $map{broker} = xml_string(xml_element("broker", \%attr, @body), %xmlopt);
    # generate the list of things to be imported
    $map{imports} = "";
    if ($Config{connector}{jetty}) {
        foreach my $xml (
            xml_comment("enable the web console and jmx4perl"),
            xml_element("import", { resource => "jetty.xml" }),
            ) {
            $map{imports} .= xml_string($xml, %xmlopt);
        }
    }
    # collate everything
    $path = "$Config{broker}{confdir}/activemq.xml";
    ensure_file($path, expand_template("activemq/activemq", %map));
}

#
# confdir/brkpurge.conf generation
#

sub generate_brkpurge () {
    my($connector, %map, $path);

    return unless is_true($Config{broker}{brkpurge});
    foreach my $name (sort(keys(%{ $Config{connector} }))) {
        $connector = $Config{connector}{$name};
        next unless $connector->{protocol} eq "stomp";
        next if is_true($connector->{ssl});
        $map{port} = $connector->{port};
        last;
    }
    dief("no STOMP connector found for brkpurge!") unless $map{port};
    $map{user} = $Config{user}{brkpurger} ? "brkpurger" : "system";
    $map{password} = $Config{user}{$map{user}}{password};
    dief("no password found for brkpurger!") unless $map{password};
    # purged messages get 5% of total disk space with min=1GB and max=8GB
    $map{maxsize} = int($Config{quota}{disk} * 5 / 100 / GB + 0.5);
    if ($map{maxsize} < 1) {
        $map{maxsize} = 1;
    } elsif ($map{maxsize} > 8) {
        $map{maxsize} = 8;
    }
    $path = "$Config{broker}{confdir}/brkpurge.conf";
    ensure_file($path, expand_template("activemq/brkpurge", %map));
}

#
# cron.d file generation
#

sub generate_cron () {
    my($daily, $hourly, %entry, $size, $contents);

    $daily = "<{rnd60}> 1 * * *";
    $hourly = "<{rnd60}> * * * *";
    $entry{mailto} = $Config{broker}{mailto}
        if defined($Config{broker}{mailto});
    $entry{keystore} = sprintf("%s %s %s/update-keystore >/dev/null 2>&1",
        $daily, "root", $Config{broker}{bindir});
    if (is_true($Config{broker}{brkpurge})) {
        $entry{brkpurge} = sprintf("%s %s brkpurge --config %s/brkpurge.conf",
            $hourly, $Config{broker}{user}, $Config{broker}{confdir});
    }
    if (is_true($Config{broker}{logpurge})) {
        # logs get 1% of total disk space with min=1GB and max=4GB
        $size = int($Config{quota}{disk} / 100 / GB + 0.5);
        if ($size < 1) {
            $size = 1;
        } elsif ($size > 4) {
            $size = 4;
        }
        $entry{logpurge} = sprintf("%s %s logpurge --maxsize %d %s",
            $hourly, $Config{broker}{user}, $size, $Config{broker}{logdir});
    }
    $contents = format_crontab(%entry);
    ensure_file("/etc/cron.d/mb-$Config{broker}{name}", $contents);
}

#
# jaas files generation
#

sub generate_jaas () {
    my($path, $ignore);

    $path = "$Config{broker}{confdir}/$Config{jaas}{config}";
    ensure_file($path, expand_template("activemq/login"));
    $path = "$Config{broker}{confdir}/$Config{jaas}{users}";
    ensure_file($path, Config::Generator::jaas::generate_users());
    $path = "$Config{broker}{confdir}/$Config{jaas}{groups}";
    $ignore = qr/^(consoles|monitors)$/;
    ensure_file($path, Config::Generator::jaas::generate_groups($ignore));
    $path = "$Config{broker}{confdir}/$Config{jaas}{dns}";
    ensure_file($path, Config::Generator::jaas::generate_dns());
}

#
# jetty files generation
#

sub generate_jetty () {
    my($path, %map, $contents);

    return unless $Config{connector}{jetty};
    # backward compatibility with old Jetty 8
    $map{jetty8} = 1 if $Config{broker}{homedir} =~ /redhat-62/;
    $path = "$Config{broker}{confdir}/jetty.xml";
    ensure_file($path, expand_template("activemq/jetty", %map));
    $path = "$Config{broker}{confdir}/credentials.properties";
    $contents = "";
    $contents .= sprintf("activemq.username=%s\n",
                         "jetty");
    $contents .= sprintf("activemq.password=%s\n",
                         $Config{user}{jetty}{password});
    ensure_file($path, $contents);
}

#
# jmx4perl/jolokia files generation
#

sub generate_jmx4perl () {
    my($path);

    return unless $Config{connector}{jmx4perl};
    $path = "$Config{broker}{confdir}/jolokia-access.xml";
    ensure_file($path, expand_template("activemq/jolokia-access"));
}

#
# jmxremote files generation
#

sub generate_jmxremote () {
    my($path);

    return unless $Config{jmxremote};
    $path = "$Config{broker}{confdir}/$Config{jmxremote}{access}";
    ensure_file($path, Config::Generator::jmxremote::generate_access());
    $path = "$Config{broker}{confdir}/$Config{jmxremote}{password}";
    ensure_file($path, Config::Generator::jmxremote::generate_password());
}

#
# bindir/update-keystore generation
#

sub generate_keystore () {
    my($path);

    $path = "$Config{broker}{bindir}/update-keystore";
    ensure_file($path, expand_template("activemq/update-keystore"));
    ensure_mode($path, oct(755));
}

#
# confdir/log4j.properties generation
#

sub generate_log4j () {
    my($path);

    $path = "$Config{broker}{confdir}/log4j.properties";
    ensure_file($path, expand_template("activemq/log4j"));
}

#
# confdir/profile generation
#

sub generate_profile () {
    my($path, @extra, $contents);

    $path = "$Config{broker}{confdir}/activemq.profile";
    push(@extra, RUN_AS_USER => $Config{broker}{user});
    $contents = format_profile(
        "#"           => "",
        "#"           => "ActiveMQ Profile ($Config{broker}{name})",
        "#"           => "",
        APP_NAME      => "mb-$Config{broker}{name}",
        APP_LONG_NAME => "ActiveMQ Broker (\${APP_NAME})",
        ACTIVEMQ_HOME => $Config{broker}{homedir},
        ACTIVEMQ_BASE => $Config{broker}{basedir},
        ACTIVEMQ_CONF => $Config{broker}{confdir},
        WRAPPER_CMD   => "\${ACTIVEMQ_CONF}/home/bin/wrapper",
        WRAPPER_CONF  => "\${ACTIVEMQ_CONF}/wrapper.conf",
        PIDDIR        => $Config{broker}{tmpdir},
        @extra,
    );
    ensure_file($path, $contents);
}

#
# bindir/service generation
#

sub generate_service () {
    my($path);

    $path = "$Config{broker}{bindir}/service";
    ensure_file($path, expand_template("activemq/service"));
    ensure_mode($path, oct(755));
}

#
# confdir/wrapper.conf generation
#

sub generate_wrapper () {
    my($path, @list, $tmp, $template, $index);

    $path = "$Config{broker}{confdir}/wrapper.conf";
    push(@list, "-Dhawtio.authenticationEnabled=false")
        if $Config{connector}{jmx4perl};
    push(@list, list_of($Config{java}{additional}));
    $tmp = "";
    if (@list) {
        $template = read_template("activemq/wrapper");
        $index = 0;
        while ($template =~ /^\s*wrapper\.java\.additional\.(\d+)\s*=/mg) {
            $index = $1 unless $index >= $1;
        }
        dief("invalid wrapper.conf template: no wrapper.java.additional lines")
            unless $index;
        log_debug("wrapper.java.additional last index is %d", $index);
        foreach my $string (@list) {
            $tmp .= sprintf("wrapper.java.additional.%d=%s\n",
                            ++$index, $string);
        }
    }
    ensure_file($path, expand_template("activemq/wrapper",
        WRAPPER_JAVA_ADDITIONAL => $tmp,
    ));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_activemq);
register_hook("generate", \&generate_brkpurge);
#register_hook("generate", \&generate_cron);
register_hook("generate", \&generate_jaas);
register_hook("generate", \&generate_jetty);
register_hook("generate", \&generate_jmx4perl);
register_hook("generate", \&generate_jmxremote);
#register_hook("generate", \&generate_keystore);
register_hook("generate", \&generate_log4j);
register_hook("generate", \&generate_profile);
register_hook("generate", \&generate_service);
#register_hook("generate", \&generate_wrapper);

1;
