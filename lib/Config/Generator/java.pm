#+##############################################################################
#                                                                              #
# File: Config/Generator/java.pm                                               #
#                                                                              #
# Description: Java abstraction                                                #
#                                                                              #
#-##############################################################################

# $Id: java.pm 2706 2016-07-14 13:26:53Z c0ns $

#
# module definition
#

package Config::Generator::java;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Util qw(MB list_of);
# dependencies
use Config::Generator::quota qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/quota" schema
    extend_schema("/quota", {
        memory_heap => DEF_NUMBER,
    });
    # define the "/java" schema
    register_schema("/java", {
        type => "struct",
        fields => {
            command => DEF_STRING,
            additional => OPT_STRING_LIST,
        },
    });
    # declare the "/java" subtree as mandatory
    mandatory_subtree("/java");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg, $size);

    $Config{quota}{memory_heap} ||= 0.8;
    $Config{java} ||= {};
    $cfg = $Config{java};
    $cfg->{command} ||= "java";
    unless (grep(/^-Xm[xs]\d/, list_of($cfg->{additional}))) {
        # default heap parameters come from memory_heap
        $size = $Config{quota}{memory} * $Config{quota}{memory_heap} / MB;
        push(@{ $cfg->{additional} },
             "-Xms" . int($size / 2) . "m",
             "-Xmx" . int($size) . "m",
        );
    }
}

#
# registration
#

init();
register_hook("check", \&check);

1;
