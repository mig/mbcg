#+##############################################################################
#                                                                              #
# File: Config/Generator/artemis/ssl.pm                                        #
#                                                                              #
# Description: Artemis SSL abstraction                                         #
#                                                                              #
#-##############################################################################

# $Id: ssl.pm 2637 2016-05-23 13:22:24Z c0ns $

#
# module definition
#

package Config::Generator::artemis::ssl;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
# dependencies
use Config::Generator::broker qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/broker" schema to add an optional "ssl" field
    extend_schema("/broker", {
        ssl => {
            type => "struct",
            fields => {
                keyStore => DEF_STRING,
                keyStorePassword => DEF_STRING,
                trustStore => DEF_STRING,
                trustStorePassword => DEF_STRING,
            },
            optional => "incfg",
        },
    });
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $Config{broker}{ssl} ||= {};
    $cfg = $Config{broker}{ssl};
    $cfg->{keyStore} ||= "$Config{broker}{confdir}/broker.ks";
    $cfg->{keyStorePassword} ||= "password";
    $cfg->{trustStore} ||= "$Config{broker}{confdir}/broker.ts";
    $cfg->{trustStorePassword} ||= "password";
}

#
# registration
#

init();
register_hook("check", \&check);

1;
