#+##############################################################################
#                                                                              #
# File: Config/Generator/artemis/connector.pm                                  #
#                                                                              #
# Description: Artemis connector abstraction                                   #
#                                                                              #
#-##############################################################################

# $Id: connector.pm 2671 2016-06-20 08:50:32Z c0ns $

#
# module definition
#

package Config::Generator::artemis::connector;
use strict;
use warnings;

#
# used modules
#

use No::Worries::Die qw(dief);
use No::Worries::Export qw(export_control);
use Config::Validator qw(expand_size is_true);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::XML qw(*);
# dependencies
use Config::Generator::artemis::ssl qw();
use Config::Generator::broker qw();
use Config::Generator::connector qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    my(%tcpopt, %sslopt);

    # TCP transport options
    %tcpopt = (
        "tcpNoDelay" => OPT_BOOLEAN,
        "tcpReceiveBufferSize" => OPT_SIZE,
        "tcpSendBufferSize" => OPT_SIZE,
        "directDeliver" => OPT_BOOLEAN,
    );
    # SSL transport options
    %sslopt = (
        "sslEnabled" => OPT_BOOLEAN,
        "keyStorePath" => OPT_STRING,
        "keyStorePassword" => OPT_STRING,
        "trustStorePath" => OPT_STRING,
        "trustStorePassword" => OPT_STRING,
        "enabledCipherSuites" => OPT_STRING,
        "enabledProtocols" => OPT_STRING,
        "needClientAuth" => OPT_BOOLEAN,
    );
    # jetty options
    register_schema("/connector/option/jetty", {
        type => "struct",
        fields => {
            "prefix" => DEF_PATH,
        },
    });
    # stomp options
    register_schema("/connector/option/stomp", {
        type => "struct",
        fields => {
            "connectionTtl" => OPT_INTEGER,
            "stompEnableMessageId" => OPT_BOOLEAN,
            %tcpopt,
            %sslopt,
         },
    });
    # extend the "/connector/entry" schema to add an optional "option" field
    extend_schema("/connector/entry", {
        option => { type => "table(string)", optional => "true" },
    });
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg, $protocol, $option);

    $cfg = $Config{connector};
    # set a default jetty prefix if needed
    if ($cfg->{jetty}) {
        $cfg->{jetty}{option}{prefix} ||= "/jolokia/";
    }
    # make sure the options match the protocol
    foreach my $name (keys(%{ $cfg })) {
        $protocol = $cfg->{$name}{protocol};
        dief("unsupported connector protocol: name=%s protocol=%s",
             $name, $protocol) unless $protocol =~ /^(jetty|stomp)$/;
        if (is_true($cfg->{$name}{ssl})) {
            $cfg->{$name}{option} ||= {};
            $cfg->{$name}{option}{sslEnabled} = "true";
            $cfg->{$name}{option}{keyStorePath} =
                "$Config{broker}{ssl}{keyStore}";
            $cfg->{$name}{option}{keyStorePassword} =
                "$Config{broker}{ssl}{keyStorePassword}";
            $cfg->{$name}{option}{trustStorePath} =
                "$Config{broker}{ssl}{trustStore}";
            $cfg->{$name}{option}{trustStorePassword} =
                "$Config{broker}{ssl}{trustStorePassword}";
        }
        $option = $cfg->{$name}{option};
        validate_data($option, "/connector/option/$protocol")
            if $option;
    }
}

#
# generate the <acceptors> part of the confdir/broker.xml file
#

sub generate_acceptors () {
    my(@children, $connector, $url, @list);

    foreach my $name (sort(keys(%{ $Config{connector} }))) {
        $connector = $Config{connector}{$name};
        next if $connector->{protocol} eq "jetty";
        $url = sprintf("tcp://%s:%d", $connector->{bind}, $connector->{port});
        @list = ("protocols=" . uc($connector->{protocol}));
        if ($connector->{option}) {
            foreach my $opt (sort(keys(%{ $connector->{option} }))) {
                push(@list, $opt . "="
                     . ($opt =~ /Size/
                        ? expand_size($connector->{option}{$opt})
                        : $connector->{option}{$opt}));
            }
        }
        $url .= "?" . join(";", @list);
        push(@children, xml_element("acceptor", { name => $name }, $url));
    }
    dief("no connectors defined!") unless @children;
    return(xml_wrap("acceptors", @children));
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{$_}++, qw(generate_acceptors));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

#
# registration
#

init();
register_hook("check", \&check);

1;
