#+##############################################################################
#                                                                              #
# File: Config/Generator/activemq/connector.pm                                 #
#                                                                              #
# Description: ActiveMQ connector abstraction                                  #
#                                                                              #
#-##############################################################################

# $Id: connector.pm 2498 2015-09-16 13:16:06Z c0ns $

#
# module definition
#

package Config::Generator::activemq::connector;
use strict;
use warnings;

#
# used modules
#

use Config::Validator qw(is_true);
use No::Worries::Die qw(dief);
use No::Worries::Export qw(export_control);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::XML qw(*);
# dependencies
use Config::Generator::connector qw();

#
# constants
#

use constant OPT_INTPAIR => {
    type => "string",
    match => qr/^\d+,\d+$/,
    optional => "true",
};

use constant OPT_STRLIST => {
    type => "string",
    match => qr/^(\w+[\+\-\.\,])*\w+$/,
    optional => "true",
};

#
# module initialization (including defining the schemas)
#

sub init () {
    my(%tcpopt, %sslopt, %stompopt);

    # TCP transport options
    # (see http://activemq.apache.org/tcp-transport-reference.html)
    %tcpopt = (
        "transport.closeAsync" => OPT_BOOLEAN,
        "transport.keepAlive" => OPT_BOOLEAN,
        "transport.soLinger" => OPT_INTEGER,
        "transport.soTimeout" => OPT_INTEGER,
        "transport.soWriteTimeout" => OPT_INTEGER,
        "transport.socketBufferSize" => OPT_INTEGER,
    );
    # SSL transport options
    # (see http://activemq.apache.org/ssl-transport-reference.html)
    %sslopt = (
        "transport.enabledCipherSuites" => OPT_STRLIST,
        "transport.enabledProtocols" => OPT_STRLIST,
        "transport.needClientAuth" => OPT_BOOLEAN,
    );
    # STOMP specific options
    # (see http://activemq.apache.org/stomp.html)
    # (see https://issues.apache.org/jira/browse/AMQ-3603)
    %stompopt = (
        "transport.defaultHeartBeat" => OPT_INTPAIR,
        "transport.hbGracePeriodMultiplier" => OPT_NUMBER,
    );
    # jmx4perl options
    register_schema("/connector/option/jmx4perl", {
        type => "struct",
        fields => {
            "prefix" => DEF_PATH,
        },
    });
    # openwire options
    register_schema("/connector/option/openwire", {
        type => "struct",
        fields => {
            %sslopt,
            %tcpopt,
            # special option to indicate the use of NIO
            "nio" => OPT_BOOLEAN,
            # special option to disable asynchronous dispatch
            "disableAsyncDispatch" => OPT_BOOLEAN,
        },
    });
    # stomp options
    register_schema("/connector/option/stomp", {
        type => "struct",
        fields => {
            %sslopt,
            %tcpopt,
            %stompopt,
            # special option to indicate the use of NIO
            "nio" => OPT_BOOLEAN,
            # special option to disable asynchronous dispatch
            "disableAsyncDispatch" => OPT_BOOLEAN,
        },
    });
    # websocket options
    register_schema("/connector/option/ws", {
        type => "struct",
        fields => {
            %sslopt,
        },
    });
    # extend the "/connector/entry" schema to add an optional "option" field
    extend_schema("/connector/entry", {
        option => { type => "table(string)", optional => "true" },
    });
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg, $protocol, $option);

    $cfg = $Config{connector};
    # add a default connector for jetty if needed
    if ($cfg->{console} or $cfg->{jmx4perl}) {
        $cfg->{jetty} ||= {
            port => 8161,
            protocol => "jetty",
            ssl => "false",
            udp => "false",
            bind => "127.0.0.1",
        };
    }
    # set a default jmx4perl prefix if needed
    if ($cfg->{jmx4perl}) {
        $cfg->{jmx4perl}{option}{prefix} ||= "/j4p/";
    }
    # make sure the options match the protocol
    foreach my $name (keys(%{ $cfg })) {
        $protocol = $cfg->{$name}{protocol};
        $option = $cfg->{$name}{option};
        if ($protocol =~ /^(jmx4perl|openwire|stomp|ws)$/) {
            validate_data($option, "/connector/option/$protocol")
                if $option;
        } elsif ($protocol =~ /^(console|jetty)$/) {
            dief("unexpected connector option: name=%s protocol=%s",
                 $name, $protocol) if $option;
        } else {
            dief("unsupported connector protocol: name=%s protocol=%s",
                 $name, $protocol);
        }
    }
}

#
# checking of the special connectors
#

sub check_special () {
    my($cfg, $protocol);

    $cfg = $Config{connector};
    # the console, jetty and jmx4perl protocols must have eponym connectors
    foreach my $name (keys(%{ $cfg })) {
        $protocol = $cfg->{$name}{protocol};
        next unless $protocol =~ /^(console|jetty|jmx4perl)$/
                     or $name =~ /^(console|jetty|jmx4perl)$/;
        dief("invalid connector name: name=%s protocol=%s", $name, $protocol)
            unless $name eq $protocol;
        # ... and they cannot use UDP
        dief("unsupported option for protocol %s: udp=true", $protocol)
            if is_true($cfg->{$name}{udp});
        # ... and they must (console) or cannot (jetty|jmx4perl) use SSL
        if ($name eq "console") {
            dief("unsupported option for protocol %s: ssl=false", $protocol)
                unless is_true($cfg->{$name}{ssl});
        } else {
            dief("unsupported option for protocol %s: ssl=true", $protocol)
                if is_true($cfg->{$name}{ssl});
        }
    }
}

#
# return the transport scheme to use
#

sub transport_scheme ($$) {
    my($name, $connector) = @_;
    my($protocol, $ssl, $nio, $scheme);

    $protocol = $connector->{protocol};
    $ssl = is_true($connector->{ssl});
    $nio = $connector->{option} && is_true($connector->{option}{nio});
    if ($protocol eq "openwire") {
        if ($ssl) {
            $scheme = $nio ? "nio+ssl" : "ssl";
        } else {
            $scheme = $nio ? "nio" : "tcp";
        }
    } elsif ($protocol eq "stomp") {
        if ($ssl) {
            $scheme = $nio ? "stomp+nio+ssl" : "stomp+ssl";
        } else {
            $scheme = $nio ? "stomp+nio" : "stomp";
        }
    } elsif ($protocol eq "ws") {
        dief("unsupported options for connector %s: ws+nio", $name) if $nio;
        $scheme = $ssl ? "wss" : "ws";
    } else {
        dief("unsupported protocol for connector %s: %s", $name, $protocol);
    }
    return($scheme);
}

#
# generate the <transportConnectors> element
#

sub generate_transportConnectors () {
    my(@children, $connector, %attr, @list);

    foreach my $name (sort(keys(%{ $Config{connector} }))) {
        $connector = $Config{connector}{$name};
        next if $connector->{protocol} =~ /^(console|jetty|jmx4perl)$/;
        %attr = ();
        $attr{name} = $name;
        $attr{uri} = sprintf("%s://%s:%d",
                             transport_scheme($name, $connector),
                             $connector->{bind},
                             $connector->{port});
        if ($connector->{option}) {
            # the normal options all have a dot in their name...
            @list = grep(/\./, keys(%{ $connector->{option} }));
            # disabling asynchronous dispatch
            # (see http://activemq.apache.org/consumer-dispatch-async.html)
            $attr{disableAsyncDispatch} = "true"
                if is_true($connector->{option}{disableAsyncDispatch});
        } else {
            @list = ();
        }
        if (@list) {
            @list = map("$_=$connector->{option}{$_}", sort(@list));
            $attr{uri} .= "?" . join("&", @list);
        }
        push(@children, xml_element("transportConnector", { %attr }));
    }
    return(xml_element("transportConnectors", undef, @children));
}

#
# check if jmxremote or one of the connectors needs SSL
#

sub needs_ssl () {
    return(1) if $Config{jmxremote} and is_true($Config{jmxremote}{ssl});
    foreach my $connector (values(%{ $Config{connector} })) {
        return(1) if is_true($connector->{ssl});
    }
    return(0);
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{$_}++, qw(generate_transportConnectors needs_ssl));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("check", \&check_special);

1;
