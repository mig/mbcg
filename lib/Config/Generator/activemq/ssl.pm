#+##############################################################################
#                                                                              #
# File: Config/Generator/activemq/ssl.pm                                       #
#                                                                              #
# Description: ActiveMQ SSL abstraction                                        #
#                                                                              #
#-##############################################################################

# $Id: ssl.pm 2077 2014-05-16 06:03:20Z c0ns $

#
# module definition
#

package Config::Generator::activemq::ssl;
use strict;
use warnings;

#
# used modules
#

use No::Worries::Export qw(export_control);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::XML qw(*);
# dependencies
use Config::Generator::broker qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/broker" schema to add an optional "ssl" field
    extend_schema("/broker", {
        ssl => {
            type => "struct",
            fields => {
                keyStore => DEF_STRING,
                keyStorePassword => DEF_STRING,
                trustStore => DEF_STRING,
                trustStorePassword => DEF_STRING,
            },
            optional => "incfg",
        },
    });
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $Config{broker}{ssl} ||= {};
    $cfg = $Config{broker}{ssl};
    $cfg->{keyStore} ||= "\${activemq.conf}/broker.ks";
    $cfg->{keyStorePassword} ||= "password";
    $cfg->{trustStore} ||= "\${activemq.conf}/broker.ts";
    $cfg->{trustStorePassword} ||= "password";
}

#
# generate the <sslContext> element
#

sub generate_sslContext () {
    return(xml_element(
        "sslContext", undef, xml_element("sslContext", $Config{broker}{ssl}),
    ));
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{$_}++, qw(generate_sslContext));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

#
# registration
#

init();
register_hook("check", \&check);

1;
