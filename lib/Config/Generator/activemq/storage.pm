#+##############################################################################
#                                                                              #
# File: Config/Generator/activemq/storage.pm                                   #
#                                                                              #
# Description: ActiveMQ storage abstraction                                    #
#                                                                              #
#-##############################################################################

# $Id: storage.pm 2701 2016-07-14 11:16:29Z c0ns $

#
# module definition
#

package Config::Generator::activemq::storage;
use strict;
use warnings;

#
# used modules
#

use No::Worries::Die qw(dief);
use No::Worries::Export qw(export_control);
use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Util qw(KB);
use Config::Generator::XML qw(*);
# dependencies
use Config::Generator::broker qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    my(%common);

    # common fields for all storage types
    %common = (type => REQ_NAME);
    # KahaDB storage (http://activemq.apache.org/kahadb.html)
    register_schema("/broker/storage/KahaDB", {
        type => "struct",
        fields => {
            %common,
            checkForCorruptJournalFiles => OPT_BOOLEAN,
            checkpointInterval => OPT_INTEGER,
            checksumJournalFiles => OPT_BOOLEAN,
            cleanupInterval => OPT_INTEGER,
            compactAcksAfterNoGC => OPT_INTEGER,
            compactAcksIgnoresStoreGrowth => OPT_BOOLEAN,
            concurrentStoreAndDispatchQueues => OPT_BOOLEAN,
            concurrentStoreAndDispatchTopics => OPT_BOOLEAN,
            directory => OPT_PATH,
            enableAckCompaction => OPT_BOOLEAN,
            enableIndexWriteAsync => OPT_BOOLEAN,
            enableJournalDiskSyncs => OPT_BOOLEAN,
            ignoreMissingJournalfiles => OPT_BOOLEAN,
            indexCacheSize => OPT_INTEGER,
            indexDirectory => OPT_PATH,
            indexWriteBatchSize => OPT_INTEGER,
        },
    });
    # LevelDB storage (http://activemq.apache.org/leveldb-store.html)
    register_schema("/broker/storage/LevelDB", {
        type => "struct",
        fields => {
            %common,
            directory => OPT_PATH,
        },
    });
    # no storage (http://activemq.apache.org/how-do-i-disable-persistence.html)
    register_schema("/broker/storage/None", {
        type => "struct",
        fields => {
            %common,
        },
    });
    # extend the "/broker" schema to add an optional "storage" field
    extend_schema("/broker", {
        storage => { type => "table(string)", optional => "incfg" },
    });
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg, $dir);

    $Config{broker}{storage} ||= {};
    $cfg = $Config{broker}{storage};
    $cfg->{type} ||= "KahaDB";
    dief("unsupported broker storage type: %s", $cfg->{type})
        unless $cfg->{type} =~ /^(KahaDB|LevelDB|None)$/;
    validate_data($cfg, "/broker/storage/$cfg->{type}");
    return if $cfg->{type} eq "None";
    $dir = lc($cfg->{type});
    if ($Config{activemq}{splitDataDirectory}) {
        $cfg->{directory} ||= "$Config{activemq}{splitDataDirectory}/$dir";
        $cfg->{indexDirectory} ||= "$Config{broker}{datadir}/$dir";
    } else {
        $cfg->{directory} ||= "$Config{broker}{datadir}/$dir";
    }
}

#
# generate the <persistenceAdapter> element
#

sub generate_persistenceAdapter () {
    my(%attr, $type, $size);

    return() if $Config{broker}{storage}{type} eq "None";
    %attr = %{ $Config{broker}{storage} };
    $type = delete($attr{type});
    if ($type eq "KahaDB") {
        if (defined($attr{indexCacheSize}) and not $attr{indexCacheSize}) {
            # indexCacheSize=0 means auto-tune at 3% of maximum heap
            $size = $Config{quota}{memory} *
                    $Config{quota}{memory_heap} * 3 / 100;
            # the unit of indexCacheSize is 4KB pages
            $attr{indexCacheSize} = int($size / (4 * KB));
        }
    }
    return(xml_element(
        "persistenceAdapter", undef, xml_element(lcfirst($type), \%attr)
    ));
}

#
# generate the <tempDataStore> element
#

sub generate_tempDataStore () {
    my(%attr);

    return() if $Config{broker}{storage}{type} eq "None";
    return() unless $Config{activemq}{splitDataDirectory};
    $attr{directory} = "$Config{activemq}{splitDataDirectory}/tempdb";
    $attr{indexDirectory} = "$Config{broker}{datadir}/tempdb";
    return(xml_wrap("tempDataStore", xml_element("pListStoreImpl", \%attr)));
}

#
# create the storage directories in case they are outside of <datadir>
#

sub generate_directories () {
    my($user, $group, $mode, @paths, %seen);

    return if $Config{broker}{storage}{type} eq "None";
    if ($Config{broker}{group}) {
        $user  = "root";
        $group = $Config{broker}{group};
        $mode  = oct(770);
    } else {
        $user  = $Config{broker}{user};
        $group = "root";
        $mode  = oct(700);
    }
    push(@paths, $Config{broker}{storage}{directory})
        if $Config{broker}{storage}{directory};
    push(@paths, $Config{broker}{storage}{indexDirectory})
        if $Config{broker}{storage}{indexDirectory};
    push(@paths, $Config{activemq}{tmpDataDirectory})
        if $Config{activemq}{tmpDataDirectory};
    push(@paths, "$Config{activemq}{splitDataDirectory}/tempdb")
        if $Config{activemq}{splitDataDirectory};
    foreach my $path (@paths) {
        next if $path =~ /^\Q$Config{broker}{datadir}\E\//;
        next if $seen{$path}++;
        ensure_directory($path);
        ensure_user($path, $user);
        ensure_group($path, $group);
        ensure_mode($path, $mode);
    }
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{"generate_$_"}++, qw(persistenceAdapter tempDataStore));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_directories);

1;
