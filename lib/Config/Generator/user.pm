#+##############################################################################
#                                                                              #
# File: Config/Generator/user.pm                                               #
#                                                                              #
# Description: user abstraction                                                #
#                                                                              #
#-##############################################################################

# $Id: user.pm 2516 2015-11-17 09:51:40Z c0ns $

#
# module definition
#

package Config::Generator::user;
use strict;
use warnings;

#
# used modules
#

use No::Worries::Die qw(dief);
use No::Worries::DN qw(dn_parse);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Util qw(list_of);

#
# constants
#

use constant USER_RE  => qr/^(\w+(\.|-+))*\w+$/;
use constant GROUP_RE => qr/^\w+$/;

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/user/entry" schema
    register_schema("/user/entry", {
        type => "struct",
        fields => {
            dn => OPT_STRING_LIST,
            password => OPT_STRING_LIST,
            group => OPT_STRING_LIST,
        },
    });
    # define the "/user" schema
    register_schema("/user", {
        type => "table(valid(/user/entry))",
        min => 1,
        match => USER_RE,
    });
    # declare the "/user" subtree as mandatory
    mandatory_subtree("/user");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($user);
    my(%seen, @list, $dn);

    foreach my $name (keys(%{ $Config{user} })) {
        $user = $Config{user}{$name};
        # handle merged entries for password or dn
        foreach my $what (qw(password dn)) {
            next unless $user->{$what} and ref($user->{$what}) eq "ARRAY";
            %seen = ();
            foreach my $tmp (list_of($user->{$what})) {
                $seen{$tmp}++;
            }
            @list = keys(%seen);
            dief("invalid user %s: multiple %ss given", $name, $what)
                unless @list == 1;
            $user->{$what} = $list[0];
        }
        # check that we have either dn or password but not both
        dief("invalid user %s: neither dn nor password set", $name)
            unless defined($user->{dn}) or defined($user->{password});
        dief("invalid user %s: both dn and password set", $name)
            if defined($user->{dn}) and defined($user->{password});
        # check the dn
        if (defined($user->{dn})) {
            eval { $dn = dn_parse($user->{dn}) };
            dief("invalid user %s: %s", $name, $@) if $@;
        }
        # check the group(s)
        if (defined($user->{group})) {
            %seen = ();
            foreach my $what (list_of($user->{group})) {
                dief("invalid user %s: invalid group name: %s", $name, $what)
                    unless $what =~ GROUP_RE;
                $seen{$what}++;
            }
            $user->{group} = [ keys(%seen) ];
        }
    }
}

#
# registration
#

init();
register_hook("check", \&check);

1;
