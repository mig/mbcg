#+##############################################################################
#                                                                              #
# File: Config/Generator/host.pm                                               #
#                                                                              #
# Description: host abstraction                                                #
#                                                                              #
#-##############################################################################

# $Id: host.pm 2077 2014-05-16 06:03:20Z c0ns $

#
# module definition
#

package Config::Generator::host;
use strict;
use warnings;

#
# used modules
#

use Net::Domain qw();
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/host" schema
    register_schema("/host", {
        type => "struct",
        fields => {
            name => DEF_HOSTNAME,
            domain => DEF_HOSTNAME,
            fqdn => DEF_HOSTNAME,
        },
    });
    # declare the "/host" subtree as mandatory
    mandatory_subtree("/host");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $Config{host} ||= {};
    $cfg = $Config{host};
    $cfg->{name} ||= Net::Domain::hostname();
    $cfg->{domain} ||= Net::Domain::hostdomain();
    $cfg->{fqdn} ||= $cfg->{name} . "." . $cfg->{domain};
}

#
# registration
#

init();
register_hook("check", \&check);

1;
