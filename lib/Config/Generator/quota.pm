#+##############################################################################
#                                                                              #
# File: Config/Generator/quota.pm                                              #
#                                                                              #
# Description: service quota abstraction                                       #
#                                                                              #
#-##############################################################################

# $Id: quota.pm 2077 2014-05-16 06:03:20Z c0ns $

#
# module definition
#

package Config::Generator::quota;
use strict;
use warnings;

#
# used modules
#

use Config::Validator qw(expand_size);
use Config::Generator qw(%Config);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/quota" schema
    register_schema("/quota", {
        type => "struct",
        fields => {
            disk => DEF_SIZE,
            memory => DEF_SIZE,
        },
    });
    # declare the "/quota" subtree as mandatory
    mandatory_subtree("/quota");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $Config{quota} ||= {};
    $cfg = $Config{quota};
    $cfg->{disk} ||= "100GB";
    $cfg->{memory} ||= "4GB";
    foreach my $name (qw(disk memory)) {
        $cfg->{$name} = expand_size($cfg->{$name});
    }
}

#
# registration
#

init();
register_hook("check", \&check);

1;
