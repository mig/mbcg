#+##############################################################################
#                                                                              #
# File: Config/Generator/rabbitmq.pm                                           #
#                                                                              #
# Description: RabbitMQ messaging broker abstraction                           #
#                                                                              #
#-##############################################################################

# $Id: rabbitmq.pm 2077 2014-05-16 06:03:20Z c0ns $

#
# module definition
#

package Config::Generator::rabbitmq;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::broker qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # (re)define what a valid "/broker/quota" subtree is
    register_schema("/broker/quota", {
        type => "struct",
        fields => {
            # inherited
            disk      => DEF_SIZE,
            memory    => DEF_SIZE,
            # extension
            disk_data => DEF_NUMBER,
            disk_log  => DEF_NUMBER,
        },
    });
    # define what a valid "/rabbitmq" subtree is
    register_schema("/rabbitmq", {
        type => "struct",
        fields => {
        },
    });
    # define that the "/rabbitmq" subtree is mandatory
    mandatory_subtree("/rabbitmq");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    # extended broker quotas
    $Config{quota}{disk_data} ||= 0.9;
    $Config{quota}{disk_log}  ||= 0.1;
}

#
# registration
#

init();
register_hook("check", \&check);

#
# testbed
#

sub test () {
    print(map("$_\n", files_spec()));
}
register_hook("generate", \&test);

1;
