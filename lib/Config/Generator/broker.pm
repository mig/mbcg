#+##############################################################################
#                                                                              #
# File: Config/Generator/broker.pm                                             #
#                                                                              #
# Description: messaging broker abstraction                                    #
#                                                                              #
#-##############################################################################

# $Id: broker.pm 2677 2016-06-22 12:40:35Z c0ns $

#
# module definition
#

package Config::Generator::broker;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/broker" schema
    register_schema("/broker", {
        type => "struct",
        fields => {
            name     => REQ_NAME,
            type     => REQ_NAME,
            version  => REQ_NAME,
            basedir  => DEF_PATH,
            bindir   => DEF_PATH,
            confdir  => DEF_PATH,
            datadir  => DEF_PATH,
            logdir   => DEF_PATH,
            tmpdir   => DEF_PATH,
            homedir  => DEF_PATH,
            user     => DEF_NAME,
            group    => OPT_NAME,
            mailto   => OPT_STRING,
            brkpurge => DEF_BOOLEAN,
            logpurge => DEF_BOOLEAN,
        },
    });
    # declare the "/broker" subtree as mandatory
    mandatory_subtree("/broker");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $cfg = $Config{broker};
    $cfg->{basedir}  ||= "/var/mb/" . $cfg->{name};
    $cfg->{bindir}   ||= $cfg->{basedir} . "/bin";
    $cfg->{confdir}  ||= $cfg->{basedir} . "/etc";
    $cfg->{datadir}  ||= $cfg->{basedir} . "/data";
    $cfg->{logdir}   ||= $cfg->{basedir} . "/log";
    $cfg->{tmpdir}   ||= $cfg->{basedir} . "/tmp";
    $cfg->{homedir}  ||= "/usr/share/" . lc($cfg->{type});
    $cfg->{user}     ||= "mb-" . $cfg->{name};
    $cfg->{brkpurge} ||= "false";
    $cfg->{logpurge} ||= "false";
}

#
# directories generation
#

sub _dir ($$) {
    my($path, $writable) = @_;
    my($mode);

    ensure_directory($path);
    if ($Config{broker}{group}) {
        ensure_user($path, "root");
        ensure_group($path, $Config{broker}{group});
        $mode = oct(700) + ($writable ? oct(70) : oct(50));
    } else {
        ensure_user($path, $Config{broker}{user});
        ensure_group($path, "root");
        $mode = $writable ? oct(700) : oct(500);
    }
    ensure_mode($path, $mode);
}

sub generate_directories () {
    _dir($Config{broker}{bindir},  0); # read-only
    _dir($Config{broker}{confdir}, 0); # read-only
    _dir($Config{broker}{datadir}, 1); # read-write
    _dir($Config{broker}{logdir},  1); # read-write
    _dir($Config{broker}{tmpdir},  1); # read-write
}

#
# home symlink generation
#

sub generate_home () {
    ensure_symlink("$Config{broker}{confdir}/home" => $Config{broker}{homedir});
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_directories);
register_hook("generate", \&generate_home);

1;
