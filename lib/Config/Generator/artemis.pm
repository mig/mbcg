#+##############################################################################
#                                                                              #
# File: Config/Generator/artemis.pm                                            #
#                                                                              #
# Description: Artemis messaging broker abstraction                            #
#                                                                              #
#-##############################################################################

# $Id: artemis.pm 2724 2016-07-21 12:51:15Z c0ns $

#
# module definition
#

package Config::Generator::artemis;
use strict;
use warnings;

#
# used modules
#

use No::Worries::Die qw(dief);
use Config::Validator qw(expand_duration expand_size is_true);
use Config::Generator qw(%Config);
use Config::Generator::Crontab qw(format_crontab);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use Config::Generator::Util qw(GB format_profile list_of);
use Config::Generator::XML qw(*);
# dependencies
use Config::Generator::artemis::connector qw(*);
use Config::Generator::broker qw();
use Config::Generator::host qw();
use Config::Generator::jaas qw();
use Config::Generator::java qw();
use Config::Generator::jmxremote qw();
use Config::Generator::quota qw();
use Config::Generator::user qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/artemis" schema
    register_schema("/artemis", {
        type => "struct",
        fields => {
            # Artemis settings (simple)
            "bindings-directory"        => OPT_STRING,
            "graceful-shutdown-enabled" => OPT_BOOLEAN,
            "graceful-shutdown-timeout" => OPT_DURATION,
            "jmx-management-enabled"    => OPT_BOOLEAN,
            "journal-buffer-timeout"    => OPT_DURATION,
            "journal-directory"         => OPT_STRING,
            "journal-file-size"         => OPT_SIZE,
            "journal-min-files"         => OPT_INTEGER,
            "journal-pool-files"        => OPT_INTEGER,
            "journal-type"              => DEF_STRING,
            "large-messages-directory"  => OPT_STRING,
            "message-counter-enabled"   => OPT_BOOLEAN,
            "paging-directory"          => OPT_STRING,
            "persistence-enabled"       => OPT_BOOLEAN,
            "populate-validated-user"   => OPT_BOOLEAN,
            # Artemis settings (xml)
            "security-settings"         => OPT_STRING,
        },
    });
    # declare the "/artemis" subtree as mandatory
    mandatory_subtree("/artemis");
    # declare the templates we use
    declare_template(map("artemis/$_", qw(bootstrap broker jolokia-access
        logging service update-keystore)));
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    # type/version checks
    dief("unsupported broker version: %s %s",
         $Config{broker}{type}, $Config{broker}{version})
        unless $Config{broker}{type} eq "Artemis"
           and $Config{broker}{version} =~ /^1\.[34]$/;
    # defaults
    $Config{artemis} ||= {};
    $cfg = $Config{artemis};
    $cfg->{"journal-type"} ||= "NIO";
    dief("unsupported journal-type: %s", $cfg->{"journal-type"})
        unless $cfg->{"journal-type"} =~ /^(ASYNCIO|NIO)$/;
}

#
# confdir/bootstrap.xml generation
#

sub generate_bootstrap () {
    my($path);

    dief("missing connector: jetty") unless $Config{connector}{jetty};
    $path = "$Config{broker}{confdir}/bootstrap.xml";
    ensure_file($path, expand_template("artemis/bootstrap"));
}

#
# confdir/broker.xml generation
#

sub xml_keyval ($$) {
    my($key, $val) = @_;

    return(xml_element($key, undef, $val));
}

sub parse_given_xml ($) {
    my($name) = @_;

    return(xml_parse("<$name>$Config{artemis}{$name}</$name>"));
}

sub generate_security () {
    if ($Config{artemis}{"security-settings"}) {
        return(parse_given_xml("security-settings"));
    } else {
        return(xml_keyval("security-enabled", "false"));
    }
}

sub generate_address () {
    return(xml_wrap("address-settings",
        xml_comment("default for catch all"),
        xml_element("address-setting", { match => "#" },
            xml_keyval("dead-letter-address", "jms.queue.DLQ"),
            xml_keyval("expiry-address", "jms.queue.ExpiryQueue"),
            xml_keyval("auto-delete-jms-queues", "false"),
            xml_keyval("auto-delete-jms-topics", "false"),
            xml_keyval("redelivery-delay", 0),
            xml_keyval("max-size-bytes", expand_size("10MB")),
            xml_keyval("page-size-bytes", expand_size("1MB")),
            xml_keyval("message-counter-history-day-limit", 10),
            xml_keyval("address-full-policy", "PAGE"),
        ),
    ));
}

sub generate_broker () {
    my(%xmlopt, %attr, @body, %map, $value, $path);

    %xmlopt = (indent => 1);
    %attr = (xmlns => "urn:activemq:core");
    push(@body, xml_blank());
    push(@body, xml_keyval("name", $Config{broker}{name}));
    foreach my $name (sort(keys(%{ $Config{artemis} }))) {
        next if $name =~ /-settings$/;
        $value = $Config{artemis}{$name};
        if ($name =~ /-size$/) {
            # bytes expected
            $value = expand_size($value);
        } elsif ($name =~ /-timeout$/) {
            # milliseconds expected
            $value = int(expand_duration($value) * 1000 + 0.5);
        }
        push(@body, xml_keyval($name, $value));
    }
    push(@body, xml_blank());
    push(@body, generate_security());
    push(@body, xml_blank());
    push(@body, generate_address());
    push(@body, xml_blank());
    push(@body, generate_acceptors());
    push(@body, xml_blank());
    $map{core} = xml_string(xml_element("core", \%attr, @body), %xmlopt);
    # collate everything
    $path = "$Config{broker}{confdir}/broker.xml";
    ensure_file($path, expand_template("artemis/broker", %map));
}

#
# cron.d files generation
#

sub generate_cron () {
    my($daily, $hourly, %entry, $size, $contents);

    $daily = "<{rnd60}> 1 * * *";
    $hourly = "<{rnd60}> * * * *";
    $entry{mailto} = $Config{broker}{mailto}
        if defined($Config{broker}{mailto});
    $entry{keystore} = sprintf("%s %s %s/update-keystore >/dev/null 2>&1",
        $daily, "root", $Config{broker}{bindir});
    if (is_true($Config{broker}{logpurge})) {
        $size = $Config{quota}{disk} * 0.1 / GB;
        $size = int($size + 0.5) || 1;
        $entry{logpurge} = sprintf("%s %s logpurge --maxsize %d %s",
            $hourly, $Config{broker}{user}, $size, $Config{broker}{logdir});
    }
    $contents = format_crontab(%entry);
    ensure_file("/etc/cron.d/mb-$Config{broker}{name}", $contents);
}

#
# confdir/login.config and confdir/*.properties generation
#

sub generate_jaas () {
    my($path);

    $path = "$Config{broker}{confdir}/$Config{jaas}{config}";
    ensure_file($path, expand_template("artemis/login"));
    $path = "$Config{broker}{confdir}/$Config{jaas}{users}";
    ensure_file($path, Config::Generator::jaas::generate_users());
    $path = "$Config{broker}{confdir}/$Config{jaas}{groups}";
    ensure_file($path, Config::Generator::jaas::generate_groups());
    $path = "$Config{broker}{confdir}/$Config{jaas}{dns}";
    ensure_file($path, Config::Generator::jaas::generate_dns());
}

#
# confdir/jolokia-access.xml generation
#

sub generate_jolokia () {
    my($path);

    $path = "$Config{broker}{confdir}/jolokia-access.xml";
    ensure_file($path, expand_template("artemis/jolokia-access"));
}

#
# jmxremote files generation
#

sub generate_jmxremote () {
    my($path);

    return unless $Config{jmxremote};
    $path = "$Config{broker}{confdir}/$Config{jmxremote}{access}";
    ensure_file($path, Config::Generator::jmxremote::generate_access());
    $path = "$Config{broker}{confdir}/$Config{jmxremote}{password}";
    ensure_file($path, Config::Generator::jmxremote::generate_password());
    # avoid "Error: Password file read access must be restricted"
    ensure_user($path, $Config{broker}{user});
    ensure_mode($path, oct(600));
}

#
# bindir/update-keystore generation
#

sub generate_keystore () {
    my($path);

    $path = "$Config{broker}{bindir}/update-keystore";
    ensure_file($path, expand_template("artemis/update-keystore"));
    ensure_mode($path, oct(755));
}

#
# confdir/logging.properties generation
#

sub generate_logging () {
    my($path);

    $path = "$Config{broker}{confdir}/logging.properties";
    ensure_file($path, expand_template("artemis/logging"));
}

#
# confdir/artemis.profile generation
#

sub generate_profile () {
    my($path, @list, $cfg, $jmxremote);

    push(@list, ARTEMIS_HOME => $Config{broker}{homedir});
    push(@list, ARTEMIS_INSTANCE => $Config{broker}{basedir});
    push(@list, ARTEMIS_NAME => "mb-$Config{broker}{name}");
    push(@list, ARTEMIS_USER => $Config{broker}{user});
    push(@list, JAVACMD => $Config{java}{command})
        if $Config{java}{command};
    push(@list, JAVA_ARGS => join(" ", list_of($Config{java}{additional})))
        if $Config{java}{additional};
    $cfg = $Config{jmxremote};
    $jmxremote = "-Dcom.sun.management.jmxremote";
    push(@list, JAVA_ARGS => join(" ",
        "\$JAVA_ARGS",
        "${jmxremote}=true",
        "${jmxremote}.port=$cfg->{rmiregistry}",
        "${jmxremote}.rmi.port=$cfg->{rmiserver}",
        "${jmxremote}.ssl=false",
        "${jmxremote}.password.file=\${ARTEMIS_INSTANCE}/etc/$cfg->{password}",
        "${jmxremote}.access.file=\${ARTEMIS_INSTANCE}/etc/$cfg->{access}",
    )) if $cfg;
    $path = "$Config{broker}{confdir}/artemis.profile";
    ensure_file($path, format_profile(@list));
}

#
# bindir/service generation
#

sub generate_service () {
    my($path);

    $path = "$Config{broker}{bindir}/service";
    ensure_file($path, expand_template("artemis/service"));
    ensure_mode($path, oct(755));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_bootstrap);
register_hook("generate", \&generate_broker);
register_hook("generate", \&generate_cron);
register_hook("generate", \&generate_jaas);
register_hook("generate", \&generate_jolokia);
register_hook("generate", \&generate_jmxremote);
register_hook("generate", \&generate_keystore);
register_hook("generate", \&generate_logging);
register_hook("generate", \&generate_profile);
register_hook("generate", \&generate_service);

1;
