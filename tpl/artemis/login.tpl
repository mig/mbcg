//
// standard JAAS configuration for dual authentication
//

PasswordLogin {
  org.apache.activemq.artemis.spi.core.security.jaas.PropertiesLoginModule required
    debug=<{jaas/debug}>
    reload=<{jaas/reload}>
    org.apache.activemq.jaas.properties.user="<{jaas/users}>"
    org.apache.activemq.jaas.properties.role="<{jaas/groups}>";
};

CertificateLogin {
  org.apache.activemq.artemis.spi.core.security.jaas.TextFileCertificateLoginModule required
    debug=<{jaas/debug}>
    reload=<{jaas/reload}>
    org.apache.activemq.jaas.textfiledn.user="<{jaas/dns}>"
    org.apache.activemq.jaas.textfiledn.role="<{jaas/groups}>";
};
