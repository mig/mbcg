<?xml version="1.0" encoding="utf-8"?>
<!--
  ~~ Jolokia security policy
  -->
<restrict>
  <!-- list of allowed hosts: only localhost -->
  <remote>
    <host>127.0.0.1</host>
    <host>localhost</host>
  </remote>
  <!-- list of allowed commands: only read-only commands -->
  <commands>
    <command>list</command>
    <command>read</command>
    <command>search</command>
    <command>version</command>
  </commands>
</restrict>
