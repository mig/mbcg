<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!--
  ~~ bootstrap Artemis configuration
  -->
<broker xmlns="http://activemq.org/schema">

  <jaas-security domain="PasswordLogin" certificate-domain="CertificateLogin"/>

  <server configuration="file:${artemis.instance}/etc/broker.xml"/>

  <!-- The web server is only bound to localhost by default -->
  <web bind="http://localhost:<{connector/jetty/port}>" path="web">
    <app url="jolokia" war="jolokia.war"/>
  </web>

</broker>
