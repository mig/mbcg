<?xml version='1.0'?>
<!--
  ~~ main Artemis configuration
  -->
<configuration xmlns="urn:activemq"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:schemaLocation="urn:activemq /schema/artemis-configuration.xsd">

  <jms xmlns="urn:activemq:jms">
    <queue name="DLQ"/>
    <queue name="ExpiryQueue"/>
  </jms>

<{core}>
</configuration>
