#!/bin/sh
#
# update the Artemis keystore from certificates in /etc/grid-security
# (this must run as root to be able to read the host certificate)
#

PATH=/root/bin:/sbin:/bin:/usr/sbin:/usr/bin
pem2jks ${1+"$@"} --update \
    --key-store "<{broker/ssl/keyStore}>" --key-password "<{broker/ssl/keyStorePassword}>" \
    --key-mode 0600 <{if(broker/user)}>--key-user <{broker/user}> <{endif(broker/user)}>\
    --trust-store "<{broker/ssl/trustStore}>" --trust-password "<{broker/ssl/trustStorePassword}>" \
    || exit 1
exit 0
