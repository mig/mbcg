#!/bin/sh
#
# chkconfig: 2345 40 20
# description: <{broker/type}> messaging broker (<{broker/name}>)
#
### BEGIN INIT INFO
# Provides: mb-<{broker/name}>
# Required-Start: $local_fs $network $remote_fs
# Required-Stop: $local_fs $network $remote_fs
# Default-Start: 2 3 4 5
# Short-Description: <{broker/type}> messaging broker (<{broker/name}>)
### END INIT INFO

. "<{broker/confdir}>/artemis.profile"
exec "${ARTEMIS_HOME}/bin/service" ${1+"$@"}
