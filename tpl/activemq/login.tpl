//
// standard JAAS configuration for dual authentication
//

activemq-domain {
  org.apache.activemq.jaas.PropertiesLoginModule required
    debug=<{jaas/debug}>
    reload=<{jaas/reload}>
    org.apache.activemq.jaas.properties.user="<{jaas/users}>"
    org.apache.activemq.jaas.properties.group="<{jaas/groups}>";
};

activemq-ssl-domain {   
  org.apache.activemq.jaas.TextFileCertificateLoginModule required
    debug=<{jaas/debug}>
    reload=<{jaas/reload}>
    org.apache.activemq.jaas.textfiledn.user="<{jaas/dns}>"
    org.apache.activemq.jaas.textfiledn.group="<{jaas/groups}>";
};
