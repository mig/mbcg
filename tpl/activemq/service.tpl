#!/bin/sh
#
# ActiveMQ Messaging Broker (<{broker/name}>)
#
. "<{broker/confdir}>/activemq.profile"
exec "${ACTIVEMQ_HOME}/bin/service" ${1+"$@"}
