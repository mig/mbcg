<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

  <bean id="contexts" class="org.eclipse.jetty.server.handler.HandlerCollection">
    <property name="handlers">
      <list>
        <bean class="org.eclipse.jetty.webapp.WebAppContext">
          <property name="contextPath" value="/admin"/>
          <property name="resourceBase" value="<{broker/confdir}>/home/webapps/admin"/>
          <property name="logUrlOnStart" value="true"/>
        </bean>
        <bean class="org.eclipse.jetty.webapp.WebAppContext">
          <property name="contextPath" value="/hawtio"/>
          <property name="war" value="<{broker/confdir}>/home/webapps/hawtio"/>
          <property name="logUrlOnStart" value="true"/>
        </bean>
      </list>
    </property>
  </bean>
<{ifdef(jetty8)}>
  <bean id="Server" class="org.eclipse.jetty.server.Server" init-method="start" destroy-method="stop">
    <property name="connectors">
      <list>
        <bean id="Connector" class="org.eclipse.jetty.server.nio.SelectChannelConnector">
          <property name="port" value="<{connector/jetty/port}>"/>
          <property name="host" value="localhost"/>
        </bean>
      </list>
    </property>
    <property name="handler">
      <bean id="handlers" class="org.eclipse.jetty.server.handler.HandlerCollection">
        <property name="handlers">
          <list>
            <ref bean="contexts"/>
          </list>
        </property>
      </bean>
    </property>
  </bean>
<{endif(jetty8)}><{ifndef(jetty8)}>
  <bean id="Server" class="org.eclipse.jetty.server.Server" destroy-method="stop">
    <property name="handler">
      <bean id="handlers" class="org.eclipse.jetty.server.handler.HandlerCollection">
        <property name="handlers">
          <list>
            <ref bean="contexts"/>
          </list>
        </property>
      </bean>
    </property>
  </bean>

  <bean id="invokeConnectors" class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
    <property name="targetObject" ref="Server"/>
    <property name="targetMethod" value="setConnectors"/>
    <property name="arguments">
      <list>
        <bean id="Connector" class="org.eclipse.jetty.server.ServerConnector">
          <constructor-arg ref="Server"/>
          <property name="host" value="localhost"/>
          <property name="port" value="<{connector/jetty/port}>"/>
        </bean>
      </list>
    </property>
  </bean>

  <bean id="configureJetty" class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
    <property name="staticMethod" value="org.apache.activemq.web.config.JspConfigurer.configureJetty"/>
    <property name="arguments">
      <list>
	<ref bean="Server"/>
	<ref bean="contexts"/>
      </list>
    </property>
  </bean>

  <bean id="invokeStart" class="org.springframework.beans.factory.config.MethodInvokingFactoryBean" 
    	depends-on="configureJetty, invokeConnectors">
    <property name="targetObject" ref="Server"/>
    <property name="targetMethod" value="start"/>  	
  </bean>
<{endif(jetty8)}>
</beans>
