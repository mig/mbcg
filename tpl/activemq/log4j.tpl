## ---------------------------------------------------------------------------
## Licensed to the Apache Software Foundation (ASF) under one or more
## contributor license agreements.  See the NOTICE file distributed with
## this work for additional information regarding copyright ownership.
## The ASF licenses this file to You under the Apache License, Version 2.0
## (the "License"); you may not use this file except in compliance with
## the License.  You may obtain a copy of the License at
## 
## http://www.apache.org/licenses/LICENSE-2.0
## 
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
## ---------------------------------------------------------------------------

# Default logger
log4j.rootLogger=INFO,default
log4j.logger.org.apache.activemq.broker.BrokerRegistry=WARN
log4j.logger.org.apache.activemq.broker.TransportConnection=WARN
log4j.logger.org.apache.activemq.spring=WARN
log4j.logger.org.apache.xbean.spring=WARN
log4j.logger.org.springframework=WARN

# When debugging or reporting problems to the ActiveMQ team,
# comment out the above lines and uncomment the next.
#log4j.rootLogger=DEBUG,default
# Or for more fine grained debug logging uncomment one of these
#log4j.logger.org.apache.activemq=DEBUG
#log4j.logger.org.apache.camel=DEBUG

# Default logging
log4j.appender.default=org.apache.log4j.RollingFileAppender
log4j.appender.default.file=<{broker/logdir}>/activemq.log
log4j.appender.default.maxFileSize=10240KB
log4j.appender.default.maxBackupIndex=100
log4j.appender.default.append=true
log4j.appender.default.layout=org.apache.log4j.PatternLayout
log4j.appender.default.layout.ConversionPattern=%d [%t] %p %c{1} - %m%n

# Separate logging for the Logging Interceptor events
log4j.logger.org.apache.activemq.broker.util.LoggingBrokerPlugin.Send=INFO,events
log4j.additivity.org.apache.activemq.broker.util.LoggingBrokerPlugin.Send=false
log4j.appender.events=org.apache.log4j.RollingFileAppender
log4j.appender.events.file=<{broker/logdir}>/events.log
log4j.appender.events.maxFileSize=10240KB
log4j.appender.events.maxBackupIndex=100
log4j.appender.events.append=true
log4j.appender.events.layout=org.apache.log4j.PatternLayout
log4j.appender.events.layout.ConversionPattern=%d [%t] %p %c{1} - %m%n

# Separate logging for the STOMP frames that may get logged
log4j.logger.org.apache.activemq.transport.stomp=INFO,stomp
log4j.additivity.org.apache.activemq.transport.stomp=false
log4j.appender.stomp=org.apache.log4j.RollingFileAppender
log4j.appender.stomp.file=<{broker/logdir}>/stomp.log
log4j.appender.stomp.maxFileSize=10240KB
log4j.appender.stomp.maxBackupIndex=100
log4j.appender.stomp.append=true
log4j.appender.stomp.layout=org.apache.log4j.PatternLayout
log4j.appender.stomp.layout.ConversionPattern=%d [%t] %p %c{1} - %m%n

# Separate logging for the KahaDB activity (only while debugging)
#log4j.logger.org.apache.activemq.store.kahadb.MessageDatabase=TRACE,kahadb
#log4j.additivity.org.apache.activemq.store.kahadb.MessageDatabase=false
#log4j.appender.kahadb=org.apache.log4j.RollingFileAppender
#log4j.appender.kahadb.file=<{broker/logdir}>/kahadb.log
#log4j.appender.kahadb.maxFileSize=10240KB
#log4j.appender.kahadb.maxBackupIndex=100
#log4j.appender.kahadb.append=true
#log4j.appender.kahadb.layout=org.apache.log4j.PatternLayout
#log4j.appender.kahadb.layout.ConversionPattern=%d [%t] %p %c{1} - %m%n
