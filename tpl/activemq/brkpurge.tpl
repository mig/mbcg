#
# brkpurge configuration
#
directory = <{broker/tmpdir}>/purged
pidfile = <{broker/tmpdir}>/brkpurge.pid
maxdays = 60
maxsize = 8
uri = stomp://localhost:<{port}>
name = <{user}>
pass = <{broker/confdir}>/users.properties
destination = /queue/ActiveMQ.DLQ
