%define real_version %((cat %{_sourcedir}/VERSION || cat %{_builddir}/VERSION || echo UNKNOWN) 2>/dev/null)
%define top_directory /opt/mbcg
%define rnd_seed %{top_directory}/cfg/random.bin

Name:		mbcg
Version:	%{real_version}
Release:	1%{?dist}
Summary:	Messaging Broker Configuration Generator
License:	GPL+ or Artistic
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tgz
Source1:	VERSION
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:	noarch
BuildRequires:	perl

%description
This is the Messaging Broker Configuration Generator (MBCG). Its purpose is
to ease the management of messaging broker configuration files by generating
them from high-level configuration items and templates. These files can be
generated directly on the broker or elsewhere and then packaged via rpm, or
deployed with rsync or cfengine...

%package utils
Summary:	Set of utilities for Messaging Broker Configuration Generator
Group:		Applications/Internet

%description utils
This package contains a set of utilities for Messaging Broker Configuration
Generator (MBCG). It should be installed where MBCG-generated files get
deployed.

%prep
%setup -q -n %{name}-%{version}

%build
make build

%install
make install INSTROOT=%{buildroot} BINDIR=%{_bindir} MANDIR=%{_mandir} TOPDIR=%{top_directory}

%clean
make clean
rm -rf %{buildroot}

%post
[ -e %{rnd_seed} ] || dd if=/dev/urandom of=%{rnd_seed} bs=1k count=1 2>/dev/null

%preun
if [ $1 = 0 ]; then
  rm -fr %{top_directory}/cfg/* %{top_directory}/run/*
fi

%files
%defattr(-,root,root,-)
%doc README* Changes
%{_bindir}/mbcg*
%{_mandir}/man?/mbcg*
%{top_directory}

%files utils
%defattr(-,root,root,-)
%{_bindir}/brkpurge
%{_bindir}/cern2dns
%{_bindir}/grid2dns
%{_bindir}/logpurge
%{_bindir}/pem2jks
%{_bindir}/ugm
%{_mandir}/man?/brkpurge*
%{_mandir}/man?/cern2dns*
%{_mandir}/man?/grid2dns*
%{_mandir}/man?/logpurge*
%{_mandir}/man?/pem2jks*
%{_mandir}/man?/ugm*
